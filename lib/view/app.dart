import 'package:flutter/material.dart';
import 'home_page.dart';


class App extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomepageScreen(),
    );
  }

}
